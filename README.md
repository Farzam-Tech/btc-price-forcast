
## Name
Bitcoin Price and Returns Forecast

## Description
In this project we will test the many time series tools in order to predict future movements in the value of Bitcoin versus the USA dollar..

## Instructions
In this notebook, we will load historical BTC-USD exchange rate data and apply time series analysis and modelling to determine if there is any predictable behaviour.

#Plotting the Settle price to check for long or short-term patterns.

To see if there is any patterns, long-term and/or short?



#Decomposition using a Hodrick-Prescott filter (decompose the settle price into trend and noise).

To see if there is any patterns, long-term and/or short?



#Forecasting returns using an ARMA model.

To see if Based on the p-value, is the model a good fit?



#Forecasting the exchange rate price using an ARIMA model.

What does the model forecast will happen to the Bitcoin in the near term?



#Forecasting volatility with GARCH.

What does the model forecast will happen to volatility in the near term?

#Linear Regression Forecasting

In this notebook, we will build a Scikit-Learn linear regression model to predict BTC/USD returns with lagged BTC/USD futures returns and categorical calendar seasonal effects (e.g., day-of-week or week-of-year seasonal effects).

Data preparation (creating returns and lagged returns, and splitting the data into training and testing data)
Fitting a linear regression model.
Making predictions using the testing data.
Out-of-sample performance.
In-sample performance.

Use the results of the linear regression analysis and modelling to answer the following question:

Does this model perform better or worse on out-of-sample data compared to in-sample data?

##Tools and techniques

Python
Pandas
Numpy
Sklearn
Hvplot



## Authors and acknowledgment
Farzam Ajili
Fajili@ryerson.ca


